import java.util.Scanner;

public class Media {

	public static void main(String[] args) {
		Scanner entrada = new Scanner (System.in);//criar o objeto
		double x,y, media;
		
		System.out.print("Digite o primeiro numero: ");
		x = entrada.nextDouble();
		System.out.print("Digite o segundo numero: ");
		y = entrada.nextDouble();
		media = (x + y) / 2.0;
		System.out.println("Media = " + media);
		entrada.close();
	}

}
